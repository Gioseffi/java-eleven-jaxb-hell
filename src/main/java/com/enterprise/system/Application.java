package com.enterprise.system;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.enterprise.system.dto.FinanDTO;
import com.enterprise.system.dto.FinanTokenDTO;
import com.enterprise.system.shared.constant.Constants;
import com.enterprise.system.shared.util.XMLUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class Application {

	public static void main(final String[] args) {
		final var installment = "946.2";
		final var date = "2021-12-20T00:00:00-03:00";
		final var lineSeparator = System.lineSeparator();
		final var finan01 = new FinanTokenDTO("526", "5200", "10", "1", installment, "AX", date, date, Constants.EMPTY,
				new FinanDTO("AIR", "VHI", installment, "CIA"));
		final var list = List.of(finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
				finan01, finan01, finan01);

		Application.LOGGER.info("{}Marshalled object:{}{}", lineSeparator, lineSeparator, XMLUtils.toXML(finan01));
		Application.LOGGER.info("{}Marshalled objects:{}{}", lineSeparator, lineSeparator,
				list.stream().map(XMLUtils::toXML).collect(Collectors.joining(lineSeparator)));
		Application.LOGGER.info("{}Marshalled objects:{}{}", lineSeparator, lineSeparator,
				list.parallelStream().map(XMLUtils::toXML).collect(Collectors.joining(lineSeparator)));
	}
}
