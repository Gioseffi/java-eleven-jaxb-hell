package com.enterprise.system.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlRootElement(name = "finan")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FinanDTO {

	@XmlAttribute
	private String prd;

	@XmlAttribute
	private String pkg;

	@XmlAttribute
	private String val;

	@XmlAttribute
	private String fty;

}
