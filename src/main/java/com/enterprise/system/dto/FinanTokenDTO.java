package com.enterprise.system.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlRootElement(name = "finans")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FinanTokenDTO {

	@XmlAttribute
	private String pln;

	@XmlAttribute
	private String ope;

	@XmlAttribute
	private String mod;

	@XmlAttribute
	private String mis;

	@XmlAttribute
	private String val;

	@XmlAttribute
	private String car;

	@XmlAttribute
	private String dti;

	@XmlAttribute
	private String dtf;

	@XmlAttribute
	private String ota;

	@XmlElement
	private FinanDTO finan;

}
