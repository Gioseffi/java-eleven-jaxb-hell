package com.enterprise.system.shared.util;

import java.io.StringWriter;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.enterprise.system.shared.constant.Constants;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class XMLUtils {

	private static final Pattern REMOVE_HEADER = Pattern.compile("\\<\\?xml(.+?)\\?\\>");

	public static <T> String toXML(final T data) {
		try {
			final var jaxbMarshaller = JAXBContext.newInstance(data.getClass()).createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			final var sw = new StringWriter();
			jaxbMarshaller.marshal(data, sw);
			return XMLUtils.REMOVE_HEADER.matcher(sw.toString()).replaceAll(Constants.EMPTY).strip();
		} catch (final JAXBException e) {
			XMLUtils.LOGGER.error("Error while converting POJO to XML. ERROR: {}.", e.getMessage(), e);
		}

		return Constants.EMPTY;
	}
}
